package INF102.lab5.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AdjacencySetTest extends GraphTest {
    
    IGraph<Integer> adjacencySet;

    @BeforeEach
    public void setup() {
        adjacencySet = new AdjacencySet<>();
        for (int i = 0; i < N_NODES; i++) {
            adjacencySet.addNode(i);
        }
    }
    
    @Test
    public void setAddEdgeTest() {
        addEdgeTest(adjacencySet);
    }

    @Test
    public void setRemoveEdgeTest() {
        removeEdgeTest(adjacencySet);
    }

    @Test
    public void setGetNeighbourhoodTest() {
        getNeighbourhoodTest(adjacencySet);
    }

    @Test
    public void setConnectedTest() {
        connectedTest(adjacencySet);
    }

    @Test
    public void setNotConnectedTest() {
        notConnectedTest(adjacencySet);
    }

    @Test
    public void setConnectedUndirectedTest() {
        connectedUndirectedTest(adjacencySet);
    }

    @Test
    public void setNeighbouringNodesAreConnectedTest() {
        neighbouringNodesAreConnectedTest(adjacencySet);
    }
    

}
