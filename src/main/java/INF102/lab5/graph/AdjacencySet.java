package INF102.lab5.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {
	
	HashMap<V, Set<V>> adj = new HashMap<>();

    @Override
    public int size() {
       return adj.size(); 
    }

    @Override
    public void addNode(V node) {
        adj.put(node, new HashSet<V>());
    }

    @Override
    public void removeNode(V node) {
        adj.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
    	adj.get(u).add(v);
        adj.get(v).add(u);
    }
    

    @Override
    public void removeEdge(V u, V v) {
        adj.get(u).remove(v);
        adj.get(v).remove(u);
    }

    @Override
    public boolean hasNode(V node) {
        return adj.containsKey(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return adj.get(u).contains(v) && adj.get(v).contains(u);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return adj.get(node);
    }

    @Override
    public boolean connected(V u, V v) {
       HashSet<V> found = new HashSet<>();
       LinkedList<V> toSearch = new LinkedList<V>();
       toSearch.add(u);
       
       for (V neigh : this.getNeighbourhood(u)) {
    	   toSearch.add(neigh);
       }
       
       while(!toSearch.isEmpty()) {
    	  V next = toSearch.removeFirst();
    	  if (next.equals(v)) return true;
    	  if (found.contains(next)) continue;
    	  found.add(next);
    	  
    	  for (V neigh : this.getNeighbourhood(next)) {
    		  toSearch.add(neigh);
    	  }
    	 }
       
       return false;
       

	}

}
